FROM golang:1.14 as builder

ARG GOARCH=amd64
ARG KANIKO_VERSION="1.3.0"

RUN mkdir -p /go/src/github.com/GoogleContainerTools \
    && wget https://github.com/GoogleContainerTools/kaniko/archive/v${KANIKO_VERSION}.tar.gz \
    && tar -C /go/src/github.com/GoogleContainerTools/ -xvzf v${KANIKO_VERSION}.tar.gz\
    && cd /go/src/github.com/GoogleContainerTools/ \
    && mv kaniko-${KANIKO_VERSION} /go/src/github.com/GoogleContainerTools/kaniko \
    && cd /go/src/github.com/GoogleContainerTools/kaniko \
    && make GOARCH=${GOARCH}

# ---

FROM debian:10-slim

RUN apt update -qq && \
    apt install -qqy --no-install-recommends git ca-certificates && \
    rm -rf /var/lib/apt/lists/*

COPY --from=builder /go/src/github.com/GoogleContainerTools/kaniko/out/executor /kaniko/executor

ENV DOCKER_CONFIG /kaniko/.docker/

ENTRYPOINT ["/kaniko/executor"]
